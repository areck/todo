 # Created by Jordan PAYET le 08/12/2019 
class CreateTodos < ActiveRecord::Migration[5.2]
  def change
    create_table :todos do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :detail
      t.datetime :completed
      t.datetime :du_date

      t.timestamps
    end
  end
end
