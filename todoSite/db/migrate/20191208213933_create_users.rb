class CreateUsers < ActiveRecord::Migration[5.2]
   # Created by Jordan PAYET le 08/12/2019 
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
  end
end
