# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
 # Created by Jordan PAYET le 08/12/2019 
User.create(email: 'test01@shawi.ca', encrypted_password: 'azerty', first_name: 'Bob', last_name: 'Federer')


Todo.create(user_id: 1, name: 'Test123', detail: 'Faire le menage', completed: DateTime.strptime("09/14/2009 08:00", "%m/%d/%Y %H:%M"), du_date: DateTime.strptime("09/14/2009 09:00", "%m/%d/%Y %H:%M"))
Todo.create(user_id: 1, name: 'Salut', detail: 'Bonjour je vais bien', completed: DateTime.strptime("09/13/2009 18:00", "%m/%d/%Y %H:%M"), du_date: DateTime.strptime("09/14/2009 19:00", "%m/%d/%Y %H:%M"))
Todo.create(user_id: 1, name: 'Important', detail: 'Examens prevu', completed: DateTime.strptime("09/16/2009 08:00", "%m/%d/%Y %H:%M"), du_date: DateTime.strptime("09/15/2009 09:00", "%m/%d/%Y %H:%M"))
Todo.create(user_id: 1, name: 'Inutile', detail: 'dormir ', completed: DateTime.strptime("09/14/2019 08:00", "%m/%d/%Y %H:%M"), du_date: DateTime.strptime("07/14/2020 09:00", "%m/%d/%Y %H:%M"))
Todo.create(user_id: 1, name: 'Retard', detail: 'Faire a manger', completed: DateTime.strptime("02/10/2015 08:00", "%m/%d/%Y %H:%M"), du_date: DateTime.strptime("09/14/2015 09:00", "%m/%d/%Y %H:%M"))
