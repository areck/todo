Rails.application.routes.draw do
   # Created by Jordan PAYET le 08/12/2019 
  get 'home/show'
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }, path_names: { sign_in: 'login', sign_out: 'logout'}
  resources :todos

  authenticated :user do
    root to: 'todos#index', as: :authenticated_root
  end

    root to: "home#show", as: :home
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
